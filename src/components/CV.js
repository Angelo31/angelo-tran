import MyNavbar from './MyNavbar';
import Footer from './Footer';

import cvimg from '../images/CV.png'

import "../styles/CV.css"

function CV() {
    return (
      <div className="App-CV">
          
          <div class="topHome">
            <MyNavbar />
          </div>
          <div className="imgBox">
            <img id="image-section" src={cvimg}  />
          </div>
              
          <Footer />
      </div>
    );
  }
  
  export default CV;