import '../styles/APropos.css';

import { Link } from "react-router-dom";

function APropos() {
    return (

        <section class="apropos_section layout_padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mx-auto">
                        <div class="detail-box">
                            <h2>
                            A PROPOS
                            </h2>
                            <p>
                                Jeune diplôme et en quête de nouvelles aventures, je mets en avant mon expertise en développement Web et ma soif d'innovation. <br/>
                                Alliant ouverture d'esprit et curiosité, je m'engage à relever avec enthousiasme les défis de demain, dans un monde où la technologie <br/> ouvre de nouvelles opportunités passionnantes pour l'innovation, la croissance et l'amélioration continue. <br/><br/>
                                Ainsi, je m'efforce à travers ces visuels, à vous montrer le résultat de ces années de travail  !<br/>
                            </p>
                            <br/>
                            <br/>
                            <section className='butt'>
                                <Link className='styledbut' to="/cv">
                                    LIRE PLUS
                                </Link>
                            </section>
                        </div>
                    </div>
                </div>
            </div>


        </section>

    );
}

export default APropos;