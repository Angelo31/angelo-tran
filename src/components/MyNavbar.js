import "../styles/MyNavbar.css"

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import { NavLink } from "react-router-dom";

function MyNavbar() {
  
  const currentRoute = window.location.pathname;
  
    return (
      <Navbar expand="lg">
        <Container>
          <Navbar.Brand className='textHover' to='/'>Angelo TRAN</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
            <Nav className="me-auto">
              <ul className="navbar-nav  ">
                <li className={(currentRoute == '/home' || currentRoute == '/') ? "nav-item active" : "nav-item"}>
                  <NavLink className="nav-link" to="/home">Accueil</NavLink>
                </li>
                <li className={(currentRoute == '/cv') ? "nav-item active" : "nav-item"}>
                  <NavLink className="nav-link" to="/cv">CV</NavLink>
                </li>
                <li className={(currentRoute == '/projets') ? "nav-item active" : "nav-item"}>
                  <NavLink className="nav-link" to="/projets">Projets</NavLink>
                </li>
                <li className={(currentRoute == '/contact') ? "nav-item active" : "nav-item"}>
                  <NavLink className="nav-link"to="/contact">Contact</NavLink>
                </li>
              </ul>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  
}

export default MyNavbar;


