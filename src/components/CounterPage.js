import Counter from './Counter';
import '../styles/CounterPage.css';


function CounterPage() {
    return (
        <section class="counter">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 mx-auto counters">
                        <Counter number={126} title="Projects" />
                        <Counter number={64} title="Clients Contents" />
                        <Counter number={18} title="Prix gagnés" />
                        <Counter number={817} title="Clients" />
                    </div>
                </div>
            </div>
        </section>
    );
}

export default CounterPage;