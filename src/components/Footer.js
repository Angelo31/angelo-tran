import '../styles/Footer.css'


function Footer() {
    return(
        <footer class="container-fluid footer_section">
            <p>
                &copy; 2023 Angelo TRAN, Tous droits réservés
            </p>
        </footer>
    );
}

export default Footer;