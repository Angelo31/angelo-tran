import '../styles/Projets.css';

import MyNavbar from './MyNavbar';
import Footer from './Footer';

import imgLOS from '../images/leagueofstones.png';
import betfriends from '../images/betfriends.png';
import yaki from '../images/yaki.png';
import pokedex from '../images/pokedex.png';
import cookapp from '../images/cookapp.png';

import MyModal from './MyModal';

import React, { useState } from 'react';

function Projets() {

  
  const [modalIsOpen, setIsOpen] = useState(false);
  const [caseModal, setCaseModal] = useState(false);
  
  function openModal(modalName) {
    setIsOpen(true);
    setCaseModal(modalName);
  }

  return (
    <div className="App-projets">
        
        <div class="topHome">
          <MyNavbar isOpen={modalIsOpen}/>
        </div>
        
        <div class="container">
          <div class="row">
            <div class="col-md-12 mx-auto">
              <h3>Mes projets</h3>              
              <hr/>

              <br/>
              <br/>

              <div className="row align-items-end">
                <div class="col-md-7 col-lg-8">
                  <div class="col-12">
                    <img className="img-projets" src={betfriends} />
                  </div>
                </div>
                <div class="col-md-5 col-lg-4">
                  <section className='description'>
                    <h2 className="h2-projets">"AWGP : Application Web de Gestion de Paris sportifs"</h2>
                    <p className='smallDesc'>Une plateforme Web permettant de parier sur les matchs à venir. Un classement permet de visualiser qui est meilleur parieur parmi ses amis !  </p>
                    <p>Une application Web développée en équipe grâce aux outils ReactJS ainsi que React Redux, Bootstrap et React Hooks. Back-end grâce FireBase.</p>

                    <a onClick={()=> openModal('BetFriends')}><p className='lireplus'>Lire plus &gt; </p></a>
                    <a href='https://betfriends.netlify.app/'><p className='acceder'>Accéder au site &gt; </p></a>

                  </section>
                </div>
              </div>

              <br/>
              <br/>

              <div className="row align-items-end">
                <div class="col-md-5 col-lg-4">
                  <section className='description'>
                    <h2 className="h2-projets">"A la découverte d'Angular grâce à mon Pokédex"</h2>
                    <p className='smallDesc'>Une plateforme Web dévéloppée à partir d'une formation géniale trouvée sur le net.  </p>
                    <p>Une application Web développée en autodidacte pour découvrir et me former sur la technologie Angular. Cette application utilise notamment les outils  Typescript, Materialize, InMemoryWebApi, Services, Modules, Component.</p>

                    <a onClick={()=> openModal('Pokédex')}><p className='lireplus'>Lire plus &gt; </p></a>
                    <a href='https://angelo-pokedex.netlify.app/'><p className='acceder'>Accéder au site &gt; </p></a>

                  </section>
                </div>
                <div class="col-md-7 col-lg-8">
                  <div class="col-12">
                    <img className="img-projets" src={pokedex} />
                  </div>
                </div>
              </div>

              <br/>
              <br/>

              <div className="row align-items-end">
                <div class="col-md-7 col-lg-8">
                  <div class="col-12">
                    <img className="img-projets" src={cookapp} />
                  </div>
                </div>
                <div class="col-md-5 col-lg-4">
                  <section className='description'>
                    <h2 className="h2-projets">"Cooking App : la gestion de nos repas simplifiée !"</h2>
                    <p className='smallDesc'>Une plateforme Web qui stocke nos meilleurs repas et calcule automatique les calories et les macro aliments. </p>
                    <p>Une application Web développée en autodidacte et en FullStack grâce aux technologies ReactJS ainsi que MongoDB, Express et NodeJs.</p>

                    <a onClick={()=> openModal('Cooking APP')}><p className='lireplus'>Lire plus &gt; </p></a>
                    <p className='acceder' style={{'font-style': 'italic'}}>Cette application est en cours de développement..</p>

                  </section>
                </div>
              </div>

              <br/>
              <br/>

              <div className="row align-items-end">
                <div class="col-md-5 col-lg-4">
                  <section className='description'>
                    <h2 className="h2-projets">"Un mashup entre Hearthstone et League of Legends : League of Stones"</h2>
                    <p className='smallDesc'>Un jeu de carte inspiré des règles du jeu de stratégie Heathstone, celui-ci s'imprégnant de l'univers des personnages de League of Legends.</p>
                    <p>Une application Web développée en équipe grâce aux outils ReactJS ainsi que React Redux, Bootstrap et React Hooks. Back-end en NodeJs.</p>

                    <a onClick={()=> openModal('League of Stones')}><p className='lireplus'>Lire plus &gt; </p></a>
                    <a href='https://leagueofstones.netlify.app/'><p className='acceder'>Accéder au site &gt; </p></a>
                    <p style={{'font-style': 'italic'}}>L'application web n'est plus entièrement fonctionnelle car l'API (back-end) n'est plus disponible..</p>
                  </section>
                </div>
                
                <div class="col-md-7 col-lg-8">
                  <div class="col-12">
                    <img className="img-projets" src={imgLOS} />
                  </div>
                </div>
              </div>

              <br/>
              <br/>
              
              <div className="row align-items-end">
                <div class="col-md-7 col-lg-8">
                  <div class="col-12">
                    <img className="img-projets" src={yaki} />
                  </div>
                </div>
                <div class="col-md-5 col-lg-4">
                  <section className='description'>
                    <h2 className="h2-projets">"Yaki : le futur des jeux sociaux"</h2>
                    <p className='smallDesc'>Un jeu social où l'objectif est de cacher son secret et découvrir les secrets de ses amis. </p>
                    <p>Une application mobile développée en React Native avec le framework Expo. Back-end en MongoDB.</p>

                    <a onClick={()=> openModal('Yaki')}><p className='lireplus'>Lire plus &gt; </p></a>
                    <p className='acceder' style={{'font-style': 'italic'}}>Cette application est en cours de développement..</p>
                  </section>
                </div>
              </div>
              
              <MyModal modalOpen={modalIsOpen} setOpen={setIsOpen} case={caseModal}/>
            </div>
          </div>
        </div>

        

        <Footer />
    </div>
  );
}

export default Projets;