import '../styles/Home.css';
import '../styles/Responsive.css';

import MyNavbar from './MyNavbar';
import MyCarousel from './MyCarousel';
import Activites from './Activites';
import APropos from './APropos';
import Reseaux from './Reseaux';
import CounterPage from './CounterPage';
import Contact from './Contact';
import Info from './Info';
import Footer from './Footer';

import 'bootstrap/dist/css/bootstrap.min.css';

function Home() {
  return (
    <div className="App">
      <div class="topHome">
        <MyNavbar />
        <MyCarousel />
      </div>
      <Activites />
      <APropos />
      <Reseaux />


      <Contact />
      <Info />
      <Footer />
    </div>
  );
}

export default Home;
