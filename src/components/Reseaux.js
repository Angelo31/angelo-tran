import '../styles/Reseaux.css'

import img1 from '../images/t1.jpg'
import img2 from '../images/t2.jpg'
import linkedin from '../images/linkedin-logo.png'
import gitlab from '../images/gitlab-logo.png'


function Reseaux() {
    return (
        <section class="reseaux_section layout_padding">
            <div class="container">
                <div class="heading_container">
                    <h2>
                        Réseaux 
                    </h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 mx-auto">
                        <div class="box">
                            <div class="name">
                                <h5>
                                    Linkedin
                                </h5>
                            </div>
                            <div class="img-box">
                                <img src={img1} alt=""/>
                            </div>
                            <div class="social_box">
                                <a class="mini-box" href="https://www.linkedin.com/in/angelo-tran-12b374195/">
                                    <img src={linkedin} alt="linkedin"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mx-auto">
                        <div class="box">
                            <div class="name">
                            <h5>
                                GitLab
                            </h5>
                            </div>
                                <div class="img-box">
                            <img src={img2} alt=""/>
                                </div>
                            <div class="social_box">
                                <a class="mini-box" href="https://gitlab.com/Angelo31">
                                    <img src={gitlab} alt="gitlab"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );

}

export default Reseaux;