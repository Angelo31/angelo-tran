import '../styles/Carousel.css';

import Carousel from 'react-bootstrap/Carousel';

import img1 from '../images/img1.jpg'
import img2 from '../images/img2.jpg'

function MyCarousel() {
    return (
        <Carousel>
          <Carousel.Item>
          <img className="position-absolute w-100 h-100" src={img1} />
            <Carousel.Caption>
              <h3>Angelo TRAN</h3>
              <p>Développeur Junior</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
          <img className="position-absolute w-100 h-100" src={img2} />
            <Carousel.Caption>
              <h3>Plateforme de création</h3>
              <p>Innovations et créativité</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      );
}

export default MyCarousel;
