import '../styles/Activites.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlaneUp } from '@fortawesome/free-solid-svg-icons'
import { faFutbol } from '@fortawesome/free-solid-svg-icons'
import { faGamepad } from '@fortawesome/free-solid-svg-icons'
import { faDumbbell } from '@fortawesome/free-solid-svg-icons'

function Activites() {
    return(

        <div>

            <section class="us_section layout_padding">
                <div class="container">
                <div class="heading_container">
                    <h2>
                        Activités
                    </h2>
                </div>

                <div class="us_container ">
                    <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="box">
                            <div class="img-box">
                                <FontAwesomeIcon icon={faDumbbell} size="2xl" style={{color: "#90233e",}} />
                            </div>
                            <div class="detail-box">
                                <h5>
                                    Musculation
                                </h5>
                                <p>
                                    4 années de pratique <br/> Création de programmes de sport
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="box">
                        <div class="img-box">
                            <FontAwesomeIcon icon={faFutbol} bounce size="2xl" style={{color: "#90233e",}}/>
                        </div>
                        <div class="detail-box">
                            <h5>
                            Football
                            </h5>
                            <p>
                            Supporter du TFC et du PSG <br/>
                            </p>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="box">
                        <div class="img-box">
                            <FontAwesomeIcon icon={faGamepad} shake size="2xl" style={{color: "#90233e",}}/>
                        </div>
                        <div class="detail-box">
                            <h5>
                            Jeux vidéos
                            </h5>
                            <p>
                            Fan de jeux compétitifs à haut niveau
                            </p>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="box">
                        <div class="img-box">
                            <FontAwesomeIcon icon={faPlaneUp} bounce size="2xl" style={{color: "#90233e",}} />
                        </div>
                        <div class="detail-box">
                            <h5>
                            Voyages
                            </h5>
                            <p>
                                Passionné par l'aventure et la découverte
                            </p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </section>
        </div>
    
    
    );
}


export default Activites;