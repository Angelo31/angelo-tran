import '../styles/Contact.css';

import img from '../images/contact-img.jpg'
import emailjs from '@emailjs/browser';

import {useRef} from "react";

function Contact() {

    const form = useRef();
    console.log(form)

    const sendEmail = (e) => {
        e.preventDefault();

        emailjs.sendForm('service_jqq3a9v', 'template_reul49c', form.current, 'EsGjFCV3ZdWLkX2tQ')
            .then((result) => {
            alert("Message envoyé avec succès !");
            e.target.reset();
            }, (error) => {
            alert("Une erreur est survenue lors de l'envoie du message..")
            }
        );
    };

    return(

        <section class="contact_section ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 px-0">
                        <div class="img-box">
                            <img src={img} alt=""/>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="form_container pr-0 pr-lg-5 mr-0 mr-lg-2">
                            <div class="heading_container">
                            <h2>
                                Me contacter
                            </h2>
                            </div>
                            <form  ref={form} onSubmit={sendEmail}>
                                <div>
                                    <input name="nom" type="text" placeholder="Votre nom" required/>
                                </div>
                                <div>
                                    <input name="email" type="email" placeholder="Votre Email" required/>
                                </div>
                                <div>
                                    <input name="telephone" type="text" placeholder="Votre numéro de téléphone" required/>
                                </div>
                                <div>
                                    <input name="message" type="text" class="message-box" placeholder="Votre message" required/>
                                </div>
                                <div class="d-flex ">
                                    <button className='buttgrow' type='submit'>
                                        Envoyer !
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Contact;