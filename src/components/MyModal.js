import '../styles/MyModal.css';

import React, { useState } from 'react';
import Modal from 'react-modal';

import betfriends1 from '../images/betfriends1.png'
import betfriends2 from '../images/betfriends2.png'
import betfriends3 from '../images/betfriends3.png'



function MyModal(props){

    const customStyles = {
        content: {
          backgroundColor:'#282c34',
        },
      };

    function closeModal() {
        props.setOpen(false);
    }

    const [content, setContent] = useState(<div> </div>);

    switch(props.case) {
        case 'BetFriends':
            return(
                <div className="mymodal">
                    <Modal
                        isOpen={props.modalOpen}
                        onRequestClose={closeModal}
                        style={customStyles}
                      >
                        <h2 className='modaltitle'>{props.case}</h2>
            
                        <br/>
                        <br/>
                        <div class="container">
                            <div class="heading_container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 mx-auto">
                                        <div class="box">
                                            <div class="img-box">
                                                <img src={betfriends3} alt=""/>
                                            </div>
                                            <br/>
                                            <div class="name">
                                                <section className='descriptionModal'>
                                                    <h2>Classement</h2>
                                                    <p>
                                                        Il s'agit d'une application Web d'un jeu dans lequel les participants paris sur le score des matchs à venir.<br/> 
                                                        Un système de classement a été mis en place : il est calculé à partir des écarts entre les scores finaux est les scores pariés par les joueurs.<br/> 
                                                    </p>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-6 mx-auto">
                                        <div class="box">
                                            <div class="img-box">
                                                <img src={betfriends2} alt=""/>
                                            </div>
                                            <br/>
                                            <div class="name">
                                                <section className='descriptionModal'>
                                                    <h2>Liste d'ami</h2>
                                                    <p>
                                                        Il s'agit d'une application Web d'un jeu dans lequel les participants paris sur le score des matchs à venir.<br/> 
                                                        Un système de classement a été mis en place : il est calculé à partir des écarts entre les scores finaux est les scores pariés par les joueurs.<br/> 
                                                    </p>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-6 mx-auto">
                                        <div class="box">
                                            <div class="img-box">
                                                <img src={betfriends1} alt=""/>
                                            </div>
                                            <br/>
                                            <div class="name">
                                                <section className='descriptionModal'>
                                                    <h2>Chat</h2>
                                                    <p>
                                                        Il s'agit d'une application Web d'un jeu dans lequel les participants paris sur le score des matchs à venir.<br/> 
                                                        Un système de classement a été mis en place : il est calculé à partir des écarts entre les scores finaux est les scores pariés par les joueurs.<br/> 
                                                    </p>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer_container">
                                <button onClick={closeModal} className='close'>Fermer</button>
                            </div>
                        </div>
                    </Modal>
                </div>
            );
        case 'League of Stones':
            return (
                <div className="mymodal">
                    <Modal
                        isOpen={props.modalOpen}
                        onRequestClose={closeModal}
                        style={customStyles}
                      >
                        <h2 className='modaltitle'>{props.case}</h2>
            
                        <br/>
                        <br/>
                        <div class="footer_container">
                            <button onClick={closeModal} className='close'>Fermer</button>
                        </div>
                    </Modal>
                </div>
            );
        case 'Yaki':
            return (
                <div className="mymodal">
                    <Modal
                        isOpen={props.modalOpen}
                        onRequestClose={closeModal}
                        style={customStyles}
                      >
                        <h2 className='modaltitle'>{props.case}</h2>
            
                        <br/>
                        <br/>
                        <div class="footer_container">
                            <button onClick={closeModal} className='close'>Fermer</button>
                        </div>
                    </Modal>
                </div>
            );
        default:
            return (
                <div className="mymodal">
                    <Modal
                        isOpen={props.modalOpen}
                        onRequestClose={closeModal}
                        style={customStyles}
                      >
                        <h2 className='modaltitle'>{props.case}</h2>
            
                        <br/>
                        <br/>
                            <div class="container">
                                NOT FOUND
                            </div>
                    </Modal>
                </div>
            );
        
    }

}

export default MyModal;