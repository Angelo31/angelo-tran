import '../styles/Info.css';

function Info() {

    return(
        <section class="info_section layout_padding2">
            <div class="container">
            <div class="info_items">
                <div class="item ">
                    <div class="img-box box-1">
                        <img src="" alt=""/>
                    </div>
                    <div class="detail-box">
                        <p>
                            Toulouse
                        </p>
                    </div>
                </div>
                <div class="item ">
                    <div class="img-box box-2">
                        <img src="" alt=""/>
                    </div>
                    <div class="detail-box">
                        <p>
                            +33 6 95 76 88 56
                        </p>
                    </div>
                </div>
                <div class="item ">
                    <div class="img-box box-3">
                        <img src="" alt=""/>
                    </div>
                    <div class="detail-box">
                        <p>
                            angelo.tran@hotmail.com
                        </p>
                    </div>
                </div>
            </div>
            </div>
        </section>

    );
}

export default Info;