import '../styles/Home.css';
import '../styles/Responsive.css';

import MyNavbar from './MyNavbar';
import Contact from './Contact';
import Footer from './Footer';
import Info from './Info';

function ContactPage() {
  return (
    <div className="App">
      <div class="topHome">
        <MyNavbar />
      </div>
      <Contact />
      <Info />
      <Footer />
    </div>
  );
}

export default ContactPage;