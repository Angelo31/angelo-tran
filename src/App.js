import React from 'react';

import Home from './components/Home';
import CV from './components/CV';
import Projets from './components/Projets';
import ContactPage from './components/ContactPage';
import { BrowserRouter, Route, Routes } from 'react-router-dom';


import "./App.css"

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/home" exact element={<Home />}component={Home} />
                <Route path="/cv" exact element={<CV />} />
                <Route path="/projets" exact element={<Projets />}  />
                <Route path="/contact" exact element={<ContactPage />}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
